package com.ryangantt.distributed;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import static com.ryangantt.distributed.CircuitBreaker.*;

public class CircuitBreakerTest {
    private static final int FAILURE_THRESHOLD = 1;
    private static final long DEFAULT_TIMEOUT = 10;
    private static final Clock DEFAULT_CLOCK = new TestClock(0);
    private static final CircuitContext DEFAULT_CONTEXT = new CircuitContext(FAILURE_THRESHOLD, DEFAULT_TIMEOUT, DEFAULT_CLOCK);

    static class TestClock implements Clock {
        private long millis;

        TestClock(final long millis) {
            this.millis = millis;
        }

        @Override
        public long getCurrentTimeMillis() {
            return millis;
        }

        void setCurrentTimeMillis(final long millis) {
            this.millis = millis;
        }
    }

    @Test
    public void givenClosedCircuit_whenInvoking_thenReturnsResult() throws Exception {
        Callable<String> cb = CircuitBreaker.of(DEFAULT_CONTEXT, () -> "result");
        String result = cb.call();

        Assert.assertEquals(result, "result");
    }

    @Test
    public void givenClosedCircuit_whenRunning_thenInvokesRunnable() throws Exception {
        AtomicInteger i = new AtomicInteger(0);

        Runnable cb = CircuitBreaker.of(DEFAULT_CONTEXT, () -> i.incrementAndGet());
        cb.run();

        Assert.assertEquals(i.get(), 1);
    }

    @Test(expectedExceptions = { OpenCircuitException.class })
    public void givenClosedCircuit_whenAfterSingleCallableFailure_thenThrowsOpenCircuit() throws Exception {
        Callable<String> cb = CircuitBreaker.of(DEFAULT_CONTEXT, () -> { throw new RuntimeException("failed!"); });

        // First failure
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (Throwable t) {
            // Pass
        }

        // Second invocation
        cb.call();
    }

    @Test
    public void givenClosedCircuit_whenAfterSingleFailureAndSingleSuccessCallable_thenRemainsClosed() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new CircuitContext(2, DEFAULT_TIMEOUT), alternateFailureSuccess());

        // First failure
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (Throwable t) {
            // Pass
        }

        // First success
        cb.call();

        // Second failure
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (Throwable t) {
            // Pass
        }

        // Second success -- circuit breaker remains open
        String result = cb.call();
        Assert.assertEquals(result, "result");
    }

    @Test
    public void givenOpenCircuit_whenCalling_thenFails() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new OpenState(DEFAULT_TIMEOUT), DEFAULT_CONTEXT, () -> "result");

        try {
            cb.call();
            Assert.fail("Circuit is open");
        } catch (OpenCircuitException e) {
            // Pass;
        }
    }


    @Test
    public void givenOpenCircuit_whenCallFails_thenBackToOpen() throws Exception {
        TestClock clock = new TestClock(0);
        CircuitContext ctx = new CircuitContext(FAILURE_THRESHOLD, DEFAULT_TIMEOUT, clock);
        Callable<String> delegate = succeedsAfterNAttempts(1);
        Callable<String> cb = CircuitBreaker.of(new OpenState(DEFAULT_TIMEOUT), ctx, delegate);
        clock.setCurrentTimeMillis(DEFAULT_TIMEOUT + 1);

        try {
            cb.call();
        } catch (OpenCircuitException e) {
            // Pass
        }

        try {
            cb.call();
            // This would pass if we'd moved to halfopen
            Assert.fail("Successful call, but moved back to open state");
        } catch (OpenCircuitException e) {
            // Pass
        }

        try {
            cb.call();
            // This would pass if we'd moved to halfopen
            Assert.fail("Successful call, but moved back to open state");
        } catch (OpenCircuitException e) {
            // Pass
        }
    }

    // In other words, we can never recover from an open circuit
    @Test
    public void givenOpenCircuit_whenValidCall_thenAlwaysFails() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new OpenState(DEFAULT_TIMEOUT), DEFAULT_CONTEXT, () -> "result");

        // First failure
        try {
            cb.call();
            Assert.fail("Circuit is open");
        } catch (OpenCircuitException e) {
            // Pass
        }

        try {
            cb.call();
            Assert.fail("Circuit is open");
        } catch (OpenCircuitException e) {
            // Pass
        }
    }

    @Test
    public void givenHalfOpenCircuit_whenValidCall_thenResultReturned() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new HalfOpenState(), new CircuitContext(1, DEFAULT_TIMEOUT), () -> "result");

        String result = cb.call();
        Assert.assertEquals(result, "result");
    }

    @Test
    public void givenHalfOpenCircuit_whenValidCall_thenCircuitClosed() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new HalfOpenState(), new CircuitContext(2, DEFAULT_TIMEOUT), alternateSuccessFailure());

        String result = cb.call();
        Assert.assertEquals(result, "result");

        // If circuit closed, we can take a failure without opening the circuit

        // First failure
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (Throwable t) {
            // Pass
        }

        // First success
        result = cb.call();
        Assert.assertEquals(result, "result");
    }

    @Test
    public void givenHalfOpenCircuit_whenInvalidCall_thenCircuitOpen() throws Exception {
        Callable<String> cb = CircuitBreaker.of(new HalfOpenState(), new CircuitContext(2, DEFAULT_TIMEOUT, DEFAULT_CLOCK), alternateFailureSuccess());

        // First failure
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (Throwable t) {
            // Pass
        }

        // First success
        try {
            cb.call();
            Assert.fail("Circuit breaker remains open after first failure in half-open state");
        } catch (OpenCircuitException e) {
            // Pass
        }
    }

    @Test
    public void givenOpenCircuit_whenSuccessfulCallAfterTimeout_thenCircuitHalfOpen() throws Exception {
        long initialTimeMillis = 0;
        long timeoutMillis = initialTimeMillis + 5_000;

        TestClock clock = new TestClock(initialTimeMillis);
        CircuitContext context = new CircuitContext(1, timeoutMillis, clock);
        Callable<String> cb = CircuitBreaker.of(new OpenState(DEFAULT_TIMEOUT), context, () -> "result");

        // First call will fail when the circuit is open
        try {
            cb.call();
            Assert.fail("Circuit is open");
        } catch (OpenCircuitException e) {
            // Pass
        }

        // Virtual wait
        clock.setCurrentTimeMillis(timeoutMillis + 1);

        // We will have moved to a half-open state after 5_000ms
        String result = cb.call();
        Assert.assertEquals(result, "result");
    }

    // The whole thing should work "end to end"
    @Test
    public void givenClosedCircuit_whenOpenAfterFailures_thenClosesAfterTimeoutAndSuccess() throws Exception {
        long initialTimeMillis = 0;
        long timeoutMillis = initialTimeMillis + 5_000;

        TestClock clock = new TestClock(initialTimeMillis);
        CircuitContext context = new CircuitContext(1, timeoutMillis, clock);
        Callable<String> cb = CircuitBreaker.of(context, succeedsAfterNAttempts(1));

        // First failure will open the circuit
        try {
            cb.call();
            Assert.fail("Circuit breaker should bubble up the exception");
        } catch (OpenCircuitException e) {
            Assert.fail("Circuit should be closed at this point");
        } catch (Throwable t) {
            // Pass
        }

        // Second call will fail since the circuit is open
        try {
            cb.call();
            Assert.fail("Circuit is open at this point");
        } catch (OpenCircuitException e) {
            // Pass
        }

        clock.setCurrentTimeMillis(timeoutMillis + 1);

        // Third call will succeed since we are in half-open state after the timeout
        String result = cb.call();
        Assert.assertEquals(result, "result");
    }

    private Callable<String> succeedsAfterNAttempts(final int n) {
        AtomicInteger i = new AtomicInteger(0);
        return () -> {
            if (i.incrementAndGet() > n) {
                return "result";
            }
            throw new RuntimeException("failure!");
        };
    }

    private Callable<String> alternateSuccessFailure() {
        AtomicInteger i = new AtomicInteger(0);
        return () -> {
            if ((i.getAndIncrement() % 2) == 0) {
                return "result";
            }
            throw new RuntimeException("failure!");
        };
    }

    private Callable<String> alternateFailureSuccess() {
        AtomicInteger i = new AtomicInteger(0);
        return () -> {
            if ((i.getAndIncrement() % 2) == 1) {
                return "result";
            }
            throw new RuntimeException("failure!");
        };
    }
}
