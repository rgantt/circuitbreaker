package com.ryangantt.distributed;

/**
 * Created by ryangantt on 7/27/16.
 */
class SystemClock implements CircuitBreaker.Clock {
    @Override
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
