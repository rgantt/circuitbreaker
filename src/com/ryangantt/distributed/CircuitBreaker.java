package com.ryangantt.distributed;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * http://martinfowler.com/bliki/CircuitBreaker.html
 *
 * @author Ryan Gantt
 */
public class CircuitBreaker implements InvocationHandler {
    interface State {
        <T> T invoke(CircuitBreaker cb, Callable<T> c);
    }

    interface Clock {
        long getCurrentTimeMillis();
    }

    private final Set<Method> toIgnore;
    private final AtomicReference<State> state;
    private final CircuitContext context;
    private final Object delegate;

    CircuitBreaker(final State initialState, final CircuitContext context, final Collection<Class<?>> toIgnore, final Object delegate) {
        state = new AtomicReference<>(initialState);
        this.context = context;
        this.delegate = delegate;

        this.toIgnore = toIgnore.stream()
            .flatMap(klazz -> Arrays.asList(klazz.getMethods()).stream())
            .collect(Collectors.toSet());
    }

    public static <T, V extends T> T of(final CircuitContext context, final V delegate) {
        return of(new ClosedState(), context, delegate);
    }

    static <T, V extends T> T of(final State initialState, final CircuitContext context, final V delegate) {
        Class<?> klazz = delegate.getClass();
        List<Class<?>> toIgnore = Arrays.asList(Object.class);
        InvocationHandler handler = new CircuitBreaker(initialState, context, toIgnore, delegate);
        return (V) Proxy.newProxyInstance(klazz.getClassLoader(), klazz.getInterfaces(), handler);
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        if (toIgnore.contains(method)) {
            return method.invoke(delegate, args);
        }
        return state.get().invoke(this, () -> method.invoke(delegate, args));
    }

    void transitionTo(final State state) {
        this.state.set(state);
    }

    static class ClosedState implements State {
        private volatile int failureCount;

        @Override
        public <T> T invoke(final CircuitBreaker cb, final Callable<T> c) {
            try {
                T ret = c.call();
                resetFailures();
                return ret;
            } catch (final Throwable t) {
                recordFailure(cb);
                throw new RuntimeException(t);
            }
        }

        synchronized void recordFailure(final CircuitBreaker cb) {
            if (++failureCount >= cb.context.getFailureThreshold()) {
                cb.transitionTo(new OpenState(cb.context.getOpenCircuitTimeout()));
            }
        }

        void resetFailures() {
            failureCount = 0;
        }
    }

    static class OpenState implements State {
        private final long retryTimeMillis;

        OpenState(final long retryTimeMillis) {
            this.retryTimeMillis = retryTimeMillis;
        }

        @Override
        public <T> T invoke(final CircuitBreaker cb, final Callable<T> c) {
            Clock clock = cb.context.getClock();
            if (clock.getCurrentTimeMillis() > retryTimeMillis) {
                try {
                    T ret = c.call();
                    cb.transitionTo(new HalfOpenState());
                    return ret;
                } catch (final Throwable t) {
                    long timeoutMillis = clock.getCurrentTimeMillis() + cb.context.getOpenCircuitTimeout();
                    cb.transitionTo(new OpenState(timeoutMillis));
                }
            }
            throw new OpenCircuitException();
        }
    }

    static class HalfOpenState implements State {
        @Override
        public <T> T invoke(final CircuitBreaker cb, final Callable<T> c) {
            try {
                T ret = c.call();
                cb.transitionTo(new ClosedState());
                return ret;
            } catch (final Throwable t) {
                long timeoutMillis = cb.context.getClock().getCurrentTimeMillis() + cb.context.getOpenCircuitTimeout();
                cb.transitionTo(new OpenState(timeoutMillis));
                throw new RuntimeException(t);
            }
        }
    }
}
