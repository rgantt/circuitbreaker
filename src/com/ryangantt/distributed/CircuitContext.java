package com.ryangantt.distributed;

/**
 * Created by ryangantt on 7/27/16.
 */
class CircuitContext {
    private final CircuitBreaker.Clock clock;
    private final int failureThreshold;
    private final long openCircuitTimeout;

    public CircuitContext(final int failureThreshold, final long openCircuitTimeout, final CircuitBreaker.Clock clock) {
        this.failureThreshold = failureThreshold;
        this.openCircuitTimeout = openCircuitTimeout;
        this.clock = clock;
    }

    public CircuitContext(final int failureThreshold, final long openCircuitTimeout) {
        this(failureThreshold, openCircuitTimeout, new SystemClock());
    }

    public int getFailureThreshold() {
        return this.failureThreshold;
    }

    public long getOpenCircuitTimeout() {
        return openCircuitTimeout;
    }

    public CircuitBreaker.Clock getClock() {
        return clock;
    }
}
